﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Array2D
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,] numberGrid = {
                {1, 2 },
                {3, 4 },
                {5, 6 }
            };
            Console.WriteLine(numberGrid[2, 1]);

            Console.ReadLine();
        }
    }
}
