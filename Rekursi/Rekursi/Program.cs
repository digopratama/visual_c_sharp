﻿using System;

namespace Rekursi
{
	class Rekursi
	{
		public static void Main (string[] args)
		{
			//int batas = 20;
			//int c = 10;
			Rekursi rek = new Rekursi();
			int a = rek.rekfak (3);
			Console.WriteLine (a);
			int d = rek.rekpang (2, 7);
			Console.WriteLine (d);
			//rek.deret(batas);
			//int b = rek.fakt (c);
			//Console.WriteLine (b);
		}
		int fakt(int a)
		{
		int n = 1;
			for (int i = 1; i <= a; i++) {
				n = n * i;
			}
			return n;		
		}
		void deret(int batas)
		{
			for (int i = 1; i <= batas; i++) {
				Console.WriteLine (i);
			}
		}
		int rekfak(int n)
		{
			if (n == 1) {
				return 1;
			} else {
				return n * rekfak (n - 1);
			}
		}
		int rekpang(int a, int b)
		{
			if (b == 0) {
				return 1;
			} else {
				return a * rekpang (a, b - 1);
			}
		}
	}
}
