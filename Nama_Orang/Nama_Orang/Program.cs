﻿using System;
using System.IO;

namespace Nama_Orang
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			MainClass p = new MainClass ();

			string nama, jenis_kelamin, agama, pekerjaan, email, no_hp, gaji; //definisi variabel

			Console.Write("Nama: ");
			nama = Console.ReadLine (); //baca string simpan ke var nama
			Console.Write("Jenis Kelamin: ");
			jenis_kelamin = Console.ReadLine ();
			Console.Write ("Agama: ");
			agama = Console.ReadLine ();
			Console.Write ("Pekerjaan: ");
			pekerjaan = Console.ReadLine ();
			Console.Write ("No HP: ");
			no_hp = Console.ReadLine ();
			Console.Write ("E-Mail: ");
			email = Console.ReadLine ();
			Console.Write ("Gaji: ");
			gaji = Console.ReadLine ();

			string gab = string.Format (" Nama saya: {0}.\n Jenis Kelamin: {1}.\n Agama: {2}.\n Pekerjaan: {3}.\n No HP: {4}.\n E-Mail: {5}.\n Gaji: {6}\n", nama, jenis_kelamin, agama, pekerjaan, email, no_hp, gaji);
			string path = "/home/digo/Code/visual_c_sharp/File/"+nama+".txt";
			p.tulisfile (gab,path);
		}

		void tulisfile(string text, string path)
		{
			try{
				StreamWriter sw = new StreamWriter(path);
				sw.WriteLine(text);
				sw.Close();
			}
			catch (Exception e){
				Console.WriteLine ("Kesalahan: " + e.Message);
			}
		}
	}
}
