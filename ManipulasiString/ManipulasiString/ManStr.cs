﻿using System;
using System.IO;

namespace ManipulasiString
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			MainClass p = new MainClass ();
			string hasil = p.bacafile ("/home/digo/Code/visual_c_sharp/File/teksteks.txt");
			Console.WriteLine (hasil.Replace(".","\n"));

			string teks = "ini teks pertama saya. Horeee";
			string path = "/home/digo/Code/visual_c_sharp/File/myfile.txt";
			p.tulisfile (teks, path);

			string nama = "saniati";
			string alamat = "Jl. Pemudi Pemuda";
			string mytext = string.Format ("Hai... nama saya {0} aja. Saya tinggal di {1}", nama,alamat);
			//string mytext = "Hello World \"saniati\" / \"riksan\". ";
			//string mytext2 = "How are you?";
			//string gab = mytext + mytext2;
			//mytext = mytext + mytext2;
			//mytext += mytext2;
			string mytext1 = string.Format("Gaji saya sebesar : {0:C}\n",2500000); //Mata Uang
			string mytext2 = string.Format("Numerik : {0:N}\n",2500000); //Numerik
			string mytext3 = string.Format("Persen : {0:P}\n",0.5); //Persentase
			string mytext4 = string.Format("Telepon : {0:+##-####-####}\n",6282180170); //Nomor Telepon
			Console.WriteLine (mytext1.ToUpper() + mytext2.ToLower() + mytext3.ToUpper() + mytext4.ToLower());
			mytext = "Rajinlah belajar, karena ilmu merupakan harta tak ternilai harganya.";
			mytext = mytext.ToUpper ();
			Console.WriteLine (mytext.Substring (0, 16));
			string textsebagian = mytext.Substring (17, 12);
			Console.WriteLine (textsebagian);
			string textalay = mytext.Replace ("A", "4");
			textalay = textalay.Replace ("E", "3");
			Console.WriteLine (textalay);

		}

		string bacafile(string path)
		{
			string temptext;
			string text = "";
			try
			{
				StreamReader sr = new StreamReader(path);
				temptext = sr.ReadLine();
				while (temptext != null)
				{
					text += temptext;
					temptext = sr.ReadLine();
				}
				sr.Close();
			}
			catch (Exception e) {
				Console.WriteLine ("Exception: " + e.Message);
			}
			return (text);
		}

		void tulisfile(string text, string path)
		{
			try{
				StreamWriter sw = new StreamWriter(path);
				sw.WriteLine(text);
				sw.Close();
			}
			catch (Exception e){
				Console.WriteLine ("Exception: " + e.Message);
			}
		}
	}
}
