﻿using System;

namespace UAS
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			MainClass pgl = new MainClass ();
			int[] array = new int[6];
			Console.Write ("Masukan nilai array index ke-0 ");
			int a = Convert.ToInt32 (Console.ReadLine ());
			array [0] = a;
			Console.Write ("Masukan nilai array index ke-1 ");
			int b = Convert.ToInt32 (Console.ReadLine ());
			array [1] = b;
			Console.Write ("Masukan nilai array index ke-2 ");
			int c = Convert.ToInt32 (Console.ReadLine ());
			array [2] = c;
			Console.Write ("Masukan nilai array index ke-3 ");
			int d = Convert.ToInt32 (Console.ReadLine ());
			array [3] = d;
			Console.Write ("Masukan nilai array index ke-4 ");
			int e = Convert.ToInt32 (Console.ReadLine ());
			array [4] = e;
			Console.Write ("Masukan nilai array index ke-5 ");
			int f = Convert.ToInt32 (Console.ReadLine ());
			array [5] = f;
			Console.Write ("Array ");
			pgl.tampilArray (array);
			pgl.sortingArray (array);
			Console.Write ("Array ");
			Console.WriteLine (" ");
			pgl.tampilArray (array);
		}

		void tampilArray(int[] ta)
		{
			for (int i = 0; i<ta.Length; i++){
			Console.Write (ta [i]+" ");
		   }
			Console.Write ("\n");
		}

		void sortingArray(int[] ra)
		{
			for (int j = 0; j < ra.Length; j++) {
				for (int i = 0; i < (ra.Length - 1); i++) {
					if (ra [i] < ra [i + 1]) {
						int temp = ra[i];
						ra [i] = ra [i + 1];
						ra [i + 1] = temp;
					}
				}
			}
		}
	}
}
