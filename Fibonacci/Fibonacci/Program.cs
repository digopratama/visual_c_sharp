﻿using System;

namespace Fibonacci
{
	class Fibonacci
	{
		public static void Main (string[] args)
		{
			Fibonacci fib = new Fibonacci();
			//Console.WriteLine ("Masukkan deret ke: ");
			//int suku = Convert.ToInt16 (Console.ReadLine ());
			//int x = fib.fibo (4);
			//Console.WriteLine (x);
			int n, hasil;
			Console.Write ("Masukkan deret: ");
			n = int.Parse (Console.ReadLine ());
			hasil = fib.deretfibo (n);
		}

		int fibo(int n)
		{
			int temp;
			int fib0 = 1; 
			int fib1 = 1;
			for(int i = 2; i <= n; i++)
			{
				temp = fib1 + fib0;
				fib0 = fib1;
				fib1 = temp;
			}
			return (fib1);
		}

		int deretfibo(int n)
		{
			int fib0 = 1;
			int fib1 = 1;
			int hasil = 0;
			Console.WriteLine(fib0+","+fib1+", ");
			for (int i = 1; i <= n; i++)
			{
				hasil = fib0 + fib1;
				Console.Write(hasil+", ");
				fib0 = fib1;
				fib1 = hasil;
			}
			return hasil;
		}
	}
}
