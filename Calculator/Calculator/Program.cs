﻿using System;

namespace Calculator
{
	class Calculator
	{
		public static void Main (string[] args)
		{
			int a, b, c, d, e, g, h; 
			float f;
			a = 100; b = 5;
			Calculator cal = new Calculator();
			c = cal.add (a, b);
			d = cal.sub (a, b);
			e = cal.mul (a, b);
			f = cal.div (a, b);
			g = cal.max (a, b);
			h = cal.min (a, b);
			Console.WriteLine ("Penjumlahan : "+c);
			Console.WriteLine ("Pengurangan : "+d);
			Console.WriteLine ("Perkalian : "+e);
			Console.WriteLine ("Pembagian : "+f);
			Console.WriteLine ("Nilai Max : "+g);
			Console.WriteLine ("Nilai Min : "+h);
		}
		int add(int num1, int num2)
		{
			int hasil;
			hasil = num1 + num2;
			return hasil;
		}
		int sub(int a1, int a2)
		{
			return(a1 - a2);
		}
		int mul(int a1, int a2)
		{
			return(a1 * a2);
		}
		float div(int a1, int a2)
		{
			return((float)a1 / (float)a2);
		}
		int max(int a1, int a2)
		{
			int hasil;
			if (a1 > a2)
				hasil = a1;
			else
				hasil = a2;
			return(hasil);
		}
		int min(int a1, int a2)
		{
			int hasil;
			if (a1 > a2)
				hasil = a2;
			else
				hasil = a1;
			return(hasil);
		}
	}
}
